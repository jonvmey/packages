#!/bin/sh

if [ -z "$1" ] || [ "$1" = "all" ]; then
  packages=dot-templater
else
  packages=$1
fi

echo "Cleaning up build directory..."

rm -rf src/ pkg/
for package in $packages; do
  rm -rf "$package" "$package"-*.pkg.tar.xz
done

for package in $packages; do
  echo "Building $package package..."
  makepkg -p "$package".pkgbuild

  echo "Installing $package package..."
  sudo pacman -U "$package"-*.pkg.tar.xz
done

